pragma solidity >= 0.7.0 <= 0.8.1;

import "@openzeppelin/contracts/access/Ownable.sol";
import './ITechnixoCoin.sol';

contract TechnixoVote is Ownable {
    struct Voter {
        address voter;
        uint voteAmount;
        bool isVoteFor;
    }

    struct Proposal {
        string description_url;
        uint voteFor;
        uint voteAgainst;
        uint ended_timestamp;
        address creator;
        bool is_ended;
        bool isVoteForWon;

    }

    mapping(uint => Voter[]) voters;

    mapping(uint => Proposal) public proposal;
    //proposal_id => voter_address => amount
    mapping(uint => mapping(address => uint)) public voterAmout;
    uint private _proposal_id;
    address public _technixoCoinContractAddress;

    event ProposalCreated(uint id);
    event VoteFor(address _voter, uint _amount);
    event VoteAgainst(address _voter, uint _amount);


    constructor(address technixoCoinContractAddress_){
        _technixoCoinContractAddress = technixoCoinContractAddress_;
    }

    function createProposal(string memory _description_url, uint _duration_seconds) public onlyOwner {
        uint _id = _proposal_id++;

        proposal[_id] = Proposal({
        description_url: _description_url,
        voteFor: 0,
        voteAgainst: 0,
        ended_timestamp: block.timestamp + _duration_seconds,
        creator: msg.sender,
        is_ended: false,
        isVoteForWon: false
        });
        emit ProposalCreated(_id);
    }

    function voteFor(uint proposal_id_, uint vote_amount_) public {
        // Proposal memory _proposal = proposal[_proposal_id];
        require(block.timestamp < proposal[proposal_id_].ended_timestamp, 'Proposal is ended');


        ITechnixoCoin(_technixoCoinContractAddress).transferFromTechnixoVote(msg.sender, vote_amount_);

        proposal[proposal_id_].voteFor += vote_amount_;
        voters[proposal_id_].push(Voter({
        voter: msg.sender,
        voteAmount: vote_amount_,
        isVoteFor: true
        }));
        emit VoteFor(msg.sender, vote_amount_);
    }

    function voteAgainst(uint proposal_id_, uint vote_amount_) public {
        // Proposal memory _proposal = proposal[_proposal_id];
        require(block.timestamp < proposal[proposal_id_].ended_timestamp, 'Proposal is ended');
        ITechnixoCoin(_technixoCoinContractAddress).transferFromTechnixoVote(msg.sender, vote_amount_);
        proposal[proposal_id_].voteAgainst += vote_amount_;
        voters[proposal_id_].push(Voter({
        voter: msg.sender,
        voteAmount: vote_amount_,
        isVoteFor: false
        }));
        emit VoteAgainst(msg.sender, vote_amount_);
    }

    function execute(uint proposal_id_) public {
        Proposal memory _proposal = proposal[proposal_id_];
        require(_proposal.creator == msg.sender, 'Only creator' );
        require(block.timestamp >= _proposal.ended_timestamp, 'Proposal is in running time');
        require(_proposal.is_ended == false, 'Proposal is ended');
        uint totalVoteAmount = _proposal.voteAgainst + _proposal.voteFor;
        bool isVoteForWon =  _proposal.voteFor*100/totalVoteAmount > 50;
        proposal[proposal_id_].isVoteForWon = isVoteForWon;
        proposal[proposal_id_].is_ended = true;
        for (uint i = 0; i < voters[proposal_id_].length; i++){
            Voter memory voter = voters[proposal_id_][i];
            ITechnixoCoin(_technixoCoinContractAddress).transferBackToVoter(voter.voter, voter.voteAmount);
        }

    }


}
