pragma solidity >= 0.7.0 <= 0.8.1;
interface ITechnixoCoin {
    function transferFromTechnixoVote(address _spender, uint _amount) external;
    function transferBackToVoter(address _receiver, uint _amount) external;
}
