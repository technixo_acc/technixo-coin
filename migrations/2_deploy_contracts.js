
const DoMathLib = artifacts.require("DoMath");
const TechnixoCoin = artifacts.require('TechnixoCoin')

module.exports = function(deployer) {
  deployer.deploy(DoMathLib)
  deployer.link(DoMathLib,TechnixoCoin)
  deployer.deploy(TechnixoCoin)
};
