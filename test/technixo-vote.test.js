const {assert} = require('chai')
const truffleAssert = require('truffle-assertions');
const TechnixoCoin = artifacts.require('TechnixoCoin')
const TechnixoVote = artifacts.require('TechnixoVote')

let technixoVoteInstance;
let technixoCoinInstance;

contract('TechnixoVote', accounts => {
    it('should create Technixo Vote instance', function () {
        return TechnixoCoin.deployed().then(
            async technixoCoinInstance => {
                assert.isObject(technixoCoinInstance)
                //pass TechnixoCoin contract address to contractor
                technixoVoteInstance = await TechnixoVote.new(technixoCoinInstance.address)
                assert.isObject(technixoVoteInstance)
                //set technixo vote contract address back to Technixo Coin
                await technixoCoinInstance.setTechnixoVoteContractAddress(technixoVoteInstance.address);
                const currentTechnixoVoteContractAddress = await technixoCoinInstance.technixoVoteContract.call();
                assert.equal(currentTechnixoVoteContractAddress, technixoVoteInstance.address)
            }
        )
    });

    it('should error when createProposal not by owner', function () {
        return truffleAssert.reverts(technixoVoteInstance.createProposal('hello', 1000, {
            from: accounts[2]
        }), 'Ownable: caller is not the owner')
    });

    it('should success create proposal when caller is owner', async function () {
        const tx = await technixoVoteInstance.createProposal('hello', 100);
        truffleAssert.eventEmitted(tx, 'ProposalCreated', ev => ev.id === 0)
    });

    //TODO test vote for
    //TODO test vote against



})
