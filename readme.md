Require:
- Truffle v5.3.1 (core: 5.3.1)
- Solidity - v0.8.1 (solc-js)
- Node v12.14.1


Run `npm install`

Test unit test

`truffle test`

Compile 

`truffle compile`

Depoloy

`truffle deploy`
