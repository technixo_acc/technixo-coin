import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import drizzleVuePlugin from '@blockrocket/drizzle-vue-plugin'
import { Drizzle } from "@drizzle/store";
import drizzleOptions from './drizzleOptions'

Vue.config.productionTip = false
// const drizzle = new Drizzle(drizzleOptions);
// const state = drizzle.store.getState();
// console.log(state)
// Register the drizzleVuePlugin
Vue.use(drizzleVuePlugin, { store, drizzleOptions })
new Vue({
  // drizzle,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
